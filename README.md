# Sound Meter

## Objective
The pourpose of this little line command component is to monitor the sound captured by
an active micro device.
A threshold is configured so there is a sound notification when the power of the sound
captured by the micro is greater than that threshold.

A practical use of this command line is to monitor your own microphone level during
remote meetings. Defining a threshold with a sound notification will help you to
maintain

## Installation
TODO

## Command line usage
TODO
