from time import sleep

import click
import sounddevice as sd
import soundfile as sf
import numpy as np

callbacks = 0
data, fs = sf.read("C:\\Windows\\Media\\Windows Default.wav", dtype="float32")
thres = 40


def play_alarm():
    sd.play(data, fs)


def print_sound(indata, outdata, frames, time, status):
    global callbacks
    volume_norm = np.linalg.norm(indata) * 20
    callbacks += 1
    if volume_norm > thres:
        print("|" * int(volume_norm) + str(volume_norm))
        play_alarm()
        sleep(1)


@click.command()
@click.option(
    "--threshold", "-t", type=click.INT, help="Sound threshold to bip!", required=True
)
def run(threshold: int):
    global thres
    thres = threshold
    with sd.Stream(
        callback=print_sound,
        samplerate=44000,
    ) as stream:
        print(f"Sample Rate: {stream.samplerate}")
        print(f"Channels: {stream.channels}")
        print(f"Device: {stream.device}")
        while True:
            sd.sleep(1000)


if __name__ == "__main__":
    run()
